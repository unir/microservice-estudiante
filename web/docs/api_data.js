define({ "api": [
  {
    "type": "get",
    "url": "1/estudiante-search",
    "title": "Busqueda de estudiantes (api_info_estudiante_search)",
    "name": "Busqueda_de_estudiantes",
    "version": "1.0.0",
    "group": "Estudiante",
    "description": "<p>Permite buscar a los estudiantes de la PUCESE.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>Token de autorización.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "search[vc_Cedula]",
            "description": "<p>ICedula del estudiante.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "search[vc_Matricula]",
            "description": "<p>Numero de matricula del estudiante.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "search[vc_Nombres]",
            "description": "<p>Nombres del estudiante.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "search[vc_Apellidos]",
            "description": "<p>Apellidos del estudiante.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Estado de la respuesta (1 es exitosa, 0 es con error).</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>Datos de respuesta.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>Numero de pagina de la busqueda.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "size",
            "description": "<p>Numero de elementos en la pagina.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "totalCount",
            "description": "<p>Numero de elementos encontrados en total.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n            \"status\": 1,\n            \"data\": [\n                {\n                    \"_id\": {\n                        \"$oid\": \"5b0f132b453a58089c00731d\"\n                    },\n                    \"vc_Matricula\": \"8888\",\n                    \"dt_Ing\": \"2012-03-30\",\n                    \"vc_Cedula\": \"0801472630\",\n                    \"vc_Tipo_Identificacion\": \"C\",\n                    \"vc_Apellidos\": \"CEVALLOS BONE\",\n                    \"vc_Nombres\": \"SONIA RAQUEL\",\n                    \"b_Categoria\": \"1\",\n                    \"vc_Sexo\": \"F\",\n                    \"dt_Nacimiento\": \"1969-07-30\",\n                    \"vc_Lugar_Nacimiento\": \"ESMERALDAS\",\n                    \"i_Id_Provincia\": \"ESMERALDAS\",\n                    \"vc_Direccion\": \"PARADA 10\",\n                    \"vc_Ciudad\": \"ESMERALDAS\",\n                    \"vc_Telefono\": \"2716-892\",\n                    \"vc_Fax\": null,\n                    \"vc_Nacionalidad\": \"ECUATORIANA\",\n                    \"vc_Colegio\": \"ELOY ALFARO\",\n                    \"vc_Ciudad_Colegio\": \"ESMERALDAS\",\n                    \"vc_Provincia_Colegio\": \"ESMERALDAS\",\n                    \"vc_Especialidad\": \"QUIMICO BIOLOGICAS\",\n                    \"i_Calificacion\": \"17\",\n                    \"vc_Otros_Titulos\": \"LIC.CC.EE ESPECILIDAD PSICOLOGIA EDUCATI\",\n                    \"b_Publicar\": \"1\",\n                    \"vc_Telefono_Familiar\": \"2720-494\",\n                    \"vc_Colegio_Procedencia\": null,\n                    \"vc_Titulo_Egresado\": null,\n                    \"vc_Tel_Familiar\": null,\n                    \"vc_Correo_Electronico\": null,\n                    \"vc_Correo_Personal\": null,\n                    \"vc_Etnia\": \"No asignada\",\n                    \"vc_Celular\": \"092426257\",\n                    \"vc_Parroquia\": \"ESMERALDAS\",\n                    \"dt_Creacion\": \"2012-03-30\",\n                    \"vc_Canton\": \"ESMERALDAS\",\n                    \"vc_Carnet_Conadis\": null,\n                    \"dt_Actualizacion\": null,\n                    \"vc_Cuenta_Facebook\": null,\n                    \"vc_Cuenta_Twitter\": null,\n                    \"i_Id_Estado_Civil\": \"0\",\n                    \"i_FK_Id_Carrera\": \"62\",\n                    \"i_Limite_Hora\": \"120\"\n                }\n            ],\n            \"page\": 1,\n            \"size\": 10,\n            \"totalCount\": 1\n        }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "status",
            "description": "<p>Estado de la respuesta.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "error_code",
            "description": "<p>Código del error.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "errors",
            "description": "<p>Lista de errores encontrados.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"status\": 0,\n    \"error_code\": 400,\n    \"errors\": [\n        \"Invalid Access token\"\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/EstudianteController.php",
    "groupTitle": "Estudiante"
  },
  {
    "type": "get",
    "url": "1/estudiante-get-nuevos",
    "title": "Estudiantes nuevos por carrera",
    "name": "Estudiantes_nuevos_por_carrera",
    "version": "1.0.0",
    "group": "Estudiante",
    "description": "<p>Permite obtener el reporte de estudiantes nuevos por carrera.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "access_token",
            "description": "<p>Token de autorización.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "search[vc_Anio]",
            "description": "<p>Anio que se desea consultar.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "search[vc_Periodo]",
            "description": "<p>I o II.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Estado de la respuesta (1 es exitosa, 0 es con error).</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>Datos de respuesta.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>Numero de pagina de la busqueda.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "size",
            "description": "<p>Numero de elementos en la pagina.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "totalCount",
            "description": "<p>Numero de elementos encontrados en total.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\n        \"status\": 1,\n        \"data\": [\n            {\n            \"_id\": \"37\",\n            \"vc_Nombre_Escuela\": \"INGENIERIA EN GESTION AMBIENTAL\",\n            \"num_estudiante\": 48\n            },\n        ],\n        \"page\": 1,\n        \"size\": 10,\n        \"totalCount\": 1\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "status",
            "description": "<p>Estado de la respuesta.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "error_code",
            "description": "<p>Código del error.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "errors",
            "description": "<p>Lista de errores encontrados.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"status\": 0,\n    \"error_code\": 400,\n    \"errors\": [\n        \"Invalid Access token\"\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/EstudianteController.php",
    "groupTitle": "Estudiante"
  },
  {
    "type": "get",
    "url": "1/estudiante",
    "title": "Inicio (api_info_estudiante_inicio)",
    "name": "Inicio",
    "version": "1.0.0",
    "group": "Estudiante",
    "description": "<p>Permite verificar si la API esta en respondiendo de manera adecuada</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Estado de la respuesta (1 es exitosa, 0 es con error).</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>Datos de respuesta.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{ \n     \"status\": 1,\n     \"data\": [\n         \"Estudiante API\"\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/EstudianteController.php",
    "groupTitle": "Estudiante"
  },
  {
    "type": "post",
    "url": "1/estudiante-migrate",
    "title": "Migrar Estudiantes(api_gest_estudiante_migrate)",
    "name": "Migrar_Estudiantes",
    "version": "1.0.0",
    "group": "Estudiante",
    "description": "<p>Permite Migrar los Estudiantes de SQL Server a MongoDB</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Estado de la respuesta (1 es exitosa, 0 es con error).</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>Datos de respuesta.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{ \n     \"status\": 1,\n     \"data\": [\n         \"creados\":45,\n         \"actualizados\":15\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/EstudianteController.php",
    "groupTitle": "Estudiante"
  }
] });
