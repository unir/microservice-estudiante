<?php

namespace app\controllers;

use Yii;
use app\models\EstudianteFuente;
use app\models\Estudiante;
use app\searchs\EstudianteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\behaviours\Verbcheck;
use app\behaviours\Apiauth;
use app\models\AccessTokens;

/**
 * ProvinciaController implements the CRUD actions for Provincia model.
 */
class EstudianteController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
                // se verifica el acces token
                // exclude =>  Indica que acciones no necesitan verificacion
                'apiauth' => [
                    'class' => Apiauth::className(),
                    'exclude' => ['index'],
                ],
                'verbs' => [
                    'class' => Verbcheck::className(),
                    'actions' => [
                        'search' => ['GET'],
                        'searchHoras' => ['GET'],
                        'migrate' => ['POST'],
                        'update' => ['POST'],
                        'get-last-matricula' => ['GET'],
                        'estudiantes-nuevos'=> ['GET']
                    ],
                ],
            ];
    }

    /**
     * @api {get} 1/estudiante Inicio (api_info_estudiante_inicio)
     * @apiName Inicio
     * @apiVersion 1.0.0
     * @apiGroup Estudiante
     * @apiDescription Permite verificar si la API esta en respondiendo de manera adecuada
     *
     * @apiSuccess {Number} status Estado de la respuesta (1 es exitosa, 0 es con error).
     * @apiSuccess {Array} data  Datos de respuesta.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     { 
     *          "status": 1,
     *          "data": [
     *              "Estudiante API"
     *          ]
     *     }
     */
    public function actionIndex()
    {
        Yii::$app->api->sendSuccessResponse(['Estudiante API']);
    }

    /**
     * @api {get} 1/estudiante-search Busqueda de estudiantes (api_info_estudiante_search)
     * @apiName Busqueda de estudiantes
     * @apiVersion 1.0.0
     * @apiGroup Estudiante
     * @apiDescription Permite buscar a los estudiantes de la PUCESE.
     *
     * @apiParam {String} access_token Token de autorización.
     * @apiParam {String} search[vc_Cedula] ICedula del estudiante.
     * @apiParam {String} search[vc_Matricula] Numero de matricula del estudiante.
     * @apiParam {String} search[vc_Nombres] Nombres del estudiante.
     * @apiParam {String} search[vc_Apellidos] Apellidos del estudiante.
     *
     * @apiSuccess {Number} status Estado de la respuesta (1 es exitosa, 0 es con error).
     * @apiSuccess {Array} data  Datos de respuesta.
     * @apiSuccess {Number} page Numero de pagina de la busqueda.
     * @apiSuccess {Number} size  Numero de elementos en la pagina.
     * @apiSuccess {Number} totalCount Numero de elementos encontrados en total.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
                "status": 1,
                "data": [
                    {
                        "_id": {
                            "$oid": "5b0f132b453a58089c00731d"
                        },
                        "vc_Matricula": "8888",
                        "dt_Ing": "2012-03-30",
                        "vc_Cedula": "0801472630",
                        "vc_Tipo_Identificacion": "C",
                        "vc_Apellidos": "CEVALLOS BONE",
                        "vc_Nombres": "SONIA RAQUEL",
                        "b_Categoria": "1",
                        "vc_Sexo": "F",
                        "dt_Nacimiento": "1969-07-30",
                        "vc_Lugar_Nacimiento": "ESMERALDAS",
                        "i_Id_Provincia": "ESMERALDAS",
                        "vc_Direccion": "PARADA 10",
                        "vc_Ciudad": "ESMERALDAS",
                        "vc_Telefono": "2716-892",
                        "vc_Fax": null,
                        "vc_Nacionalidad": "ECUATORIANA",
                        "vc_Colegio": "ELOY ALFARO",
                        "vc_Ciudad_Colegio": "ESMERALDAS",
                        "vc_Provincia_Colegio": "ESMERALDAS",
                        "vc_Especialidad": "QUIMICO BIOLOGICAS",
                        "i_Calificacion": "17",
                        "vc_Otros_Titulos": "LIC.CC.EE ESPECILIDAD PSICOLOGIA EDUCATI",
                        "b_Publicar": "1",
                        "vc_Telefono_Familiar": "2720-494",
                        "vc_Colegio_Procedencia": null,
                        "vc_Titulo_Egresado": null,
                        "vc_Tel_Familiar": null,
                        "vc_Correo_Electronico": null,
                        "vc_Correo_Personal": null,
                        "vc_Etnia": "No asignada",
                        "vc_Celular": "092426257",
                        "vc_Parroquia": "ESMERALDAS",
                        "dt_Creacion": "2012-03-30",
                        "vc_Canton": "ESMERALDAS",
                        "vc_Carnet_Conadis": null,
                        "dt_Actualizacion": null,
                        "vc_Cuenta_Facebook": null,
                        "vc_Cuenta_Twitter": null,
                        "i_Id_Estado_Civil": "0",
                        "i_FK_Id_Carrera": "62",
                        "i_Limite_Hora": "120"
                    }
                ],
                "page": 1,
                "size": 10,
                "totalCount": 1
            }
     *
     * @apiError status Estado de la respuesta.
     * @apiError error_code Código del error.
     * @apiError errors Lista de errores encontrados.
     *
     * @apiErrorExample Error-Response:
     *     {
     *         "status": 0,
     *         "error_code": 400,
     *         "errors": [
     *             "Invalid Access token"
     *         ]
     *     }
     */
    public function actionSearch()
    {
        if(isset($this->request['search'])){
            $params = $this->request['search'];
        }else{
            $params = [];
        }
        $response = EstudianteSearch::search($params);
        Yii::$app->api->sendSuccessResponse($response['data'], $response['info']);
    }

    /**
     * @api {get} 1/estudiante-get-nuevos Estudiantes nuevos por carrera
     * @apiName Estudiantes nuevos por carrera
     * @apiVersion 1.0.0
     * @apiGroup Estudiante
     * @apiDescription Permite obtener el reporte de estudiantes nuevos por carrera.
     *
     * @apiParam {String} access_token Token de autorización.
     * @apiParam {String} search[vc_Anio] Anio que se desea consultar.
     * @apiParam {String} search[vc_Periodo] I o II.
     *
     * @apiSuccess {Number} status Estado de la respuesta (1 es exitosa, 0 es con error).
     * @apiSuccess {Array} data  Datos de respuesta.
     * @apiSuccess {Number} page Numero de pagina de la busqueda.
     * @apiSuccess {Number} size  Numero de elementos en la pagina.
     * @apiSuccess {Number} totalCount Numero de elementos encontrados en total.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *{
        "status": 1,
        "data": [
            {
            "_id": "37",
            "vc_Nombre_Escuela": "INGENIERIA EN GESTION AMBIENTAL",
            "num_estudiante": 48
            },
        ],
        "page": 1,
        "size": 10,
        "totalCount": 1
    }
     *
     * @apiError status Estado de la respuesta.
     * @apiError error_code Código del error.
     * @apiError errors Lista de errores encontrados.
     *
     * @apiErrorExample Error-Response:
     *     {
     *         "status": 0,
     *         "error_code": 400,
     *         "errors": [
     *             "Invalid Access token"
     *         ]
     *     }
     */
    public function actionEstudiantesNuevos()
    {
        if(isset($_GET['search'])){
            $params = $_GET['search'];
        }else{
            $params = [];
        }
        $response = Estudiante::getEstudiantesNuevosPeriodo($params);
        header('Access-Control-Allow-Origin: *');
        Yii::$app->api->sendSuccessResponse($response, []);
    }

    public function actionSearchHoras()
    {
        if(isset($this->request['search'])){
            $params = $this->request['search'];
        }else{
            $params = [];
        }
        $response = EstudianteSearch::searchHoras($params);
        Yii::$app->api->sendSuccessResponse($response['data'], $response['info']);
    }
    /**
     * @api {post} 1/estudiante-migrate Migrar Estudiantes(api_gest_estudiante_migrate)
     * @apiName Migrar Estudiantes
     * @apiVersion 1.0.0
     * @apiGroup Estudiante
     * @apiDescription Permite Migrar los Estudiantes de SQL Server a MongoDB
     *
     * @apiSuccess {Number} status Estado de la respuesta (1 es exitosa, 0 es con error).
     * @apiSuccess {Array} data  Datos de respuesta.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     { 
     *          "status": 1,
     *          "data": [
     *              "creados":45,
     *              "actualizados":15
     *          ]
     *     }
     */
    public function actionMigrate(){
        $response = EstudianteFuente::find()->asArray(true)->all();
        $contUpdate=0;
        $contCreate=0;
        foreach ($response as $estudiante) {
            $modelEstudiante=Estudiante::find()->where(['vc_Matricula'=>$estudiante["vc_Matricula"]])->one();
            if($modelEstudiante!=null){
                $modelEstudiante->attributes=$estudiante;
                $modelEstudiante->setLimiteHora();
                $modelEstudiante->save();
                $contUpdate++;
            }else{
                $modelEstudiante= New Estudiante;
                $modelEstudiante->attributes=$estudiante;
                $modelEstudiante->setLimiteHora();
                $modelEstudiante->save();
                $contCreate++;
            }  
        }
        $response=[];
        $response['creados']=$contCreate;
        $response['actualizados']=$contUpdate;
        Yii::$app->api->sendSuccessResponse($response);
    }


    public function actionGetLastMatricula(){
        if($correo = Yii::$app->request->get('correo_institucional')){
            $response = Estudiante::getLastMatricula($correo);
            Yii::$app->api->sendSuccessResponse($response);
        }

        Yii::$app->api->sendFailedResponse(['Correo Institucional es un parametro obligatorio']);
    }

    protected function findModel($id)
    {
        if (($model = Estudiante::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionUpdate(){
        if(isset($_POST['vc_Matricula'])){
            $model = Estudiante::findOne(['vc_Matricula' => $_POST['vc_Matricula']]);
            if(isset($model)) {
                if ($model->load(Yii::$app->request->post(), 'Estudiante')) {
                    $model->i_Horas_Vinculacion=(int)$model->i_Horas_Vinculacion;
                    $model->i_Limite_Hora=(int)$model->i_Limite_Hora;
                    if ($model->save()) {
                        $model->_id=(string)$model->_id;
                        Yii::$app->api->sendSuccessResponse($model->getAttributes());
                    }else
                        Yii::$app->api->sendFailedResponse($model->getErrors());
                }
            }
        }
        Yii::$app->api->sendFailedResponse(["Debe facilitar el _id para actualizar el estudiante"]);
    }
}
