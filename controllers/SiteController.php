<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\LoginForm;
use app\models\AuthorizationCodes;
use app\models\AccessTokens;

use app\models\SignupForm;
use app\behaviours\Verbcheck;
use app\behaviours\Apiauth;

/**
 * Site controller
 */
class SiteController extends RestController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {

        $behaviors = parent::behaviors();

        return $behaviors + [
            // se verifica el acces token
            // exclude =>  Indica que acciones no necesitan verificacion
            'apiauth' => [
                'class' => Apiauth::className(),
                'exclude' => ['authorize', 'register', 'accesstoken','index'],
            ],
            // verifica el acceso a las opciones 
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['authorize', 'register', 'accesstoken'],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => Verbcheck::className(),
                'actions' => [
                    'logout' => ['GET'],
                    'authorize' => ['POST'],
                    'register' => ['POST'],
                    'accesstoken' => ['POST'],
                    'me' => ['GET'],
                ],
            ],
        ];
    }


    /*
        especifica acciones autonomas, acciones que y estan definidas
    */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /*
        index de la api
    */
    public function actionIndex()
    {
       Yii::$app->api->sendSuccessResponse(['Ubicacion API']);
        //echo "hola";
    }

    /*
        Permite registrar un usuario
    */
    // public function actionRegister()
    // {

    //     $model = new SignupForm();
    //     $model->attributes = $this->request;

    //     if ($user = $model->signup()) {

    //         $data=$user->attributes;

    //         // destruye las variables
    //         unset($data['auth_key']);
    //         unset($data['password_hash']);
    //         unset($data['password_reset_token']);

    //         Yii::$app->api->sendSuccessResponse($data);

    //     }

    // }

    /*
        Permite mostrar los datos del usuario
    */
    // public function actionMe()
    // {
    //     $data = Yii::$app->user->identity;
    //     $data = $data->attributes;

    //     // destruye las variables
    //     unset($data['auth_key']);
    //     unset($data['password_hash']);
    //     unset($data['password_reset_token']);

    //     Yii::$app->api->sendSuccessResponse($data);
    // }

    /*
        Permite obtener el access token
    */
    public function actionAccesstoken()
    {

        if (!isset($this->request["authorization_code"])) {
            Yii::$app->api->sendFailedResponse("Authorization code missing");
        }

        $authorization_code = $this->request["authorization_code"];

        /*
            verifica que el autorization code no haya expirado
            devuelve el modelo
        */
        $auth_code = AuthorizationCodes::isValid($authorization_code);

        // Si la autorizacion no existe
        if (!$auth_code) {
            Yii::$app->api->sendFailedResponse("Invalid Authorization Code");
        }

        // se crea el access token en base al autorization code
        $accesstoken = Yii::$app->api->createAccesstoken($authorization_code);

        // guardo en data, el token y la fecha de expiracion que se van a devolver como respuesta
        $data = [];
        $data['access_token'] = $accesstoken->token;
        $data['expires_at'] = $accesstoken->expires_at;
        Yii::$app->api->sendSuccessResponse($data);

    }

    /*
        Permite obtener la autorizacion
    */
    public function actionAuthorize()
    {
        //$model = new LoginForm();

        //$model->attributes = $this->request;


        //if ($model->validate() && $model->login()) {

            $auth_code = Yii::$app->api->createAuthorizationCode(1);

            $data = [];
            $data['authorization_code'] = $auth_code->code;
            $data['expires_at'] = $auth_code->expires_at;

            Yii::$app->api->sendSuccessResponse($data);
       // } else {
            //Yii::$app->api->sendFailedResponse($model->errors);
       // }
    }

    /*
        Cerrar sesion
    */
    public function actionLogout()
    {
        $headers = Yii::$app->getRequest()->getHeaders();

        //se inicializa token en null
        $accessToken=NULL;

        // se obtiene el access_token
        if(isset($_GET['access_token'])){
            $accessToken=$_GET['access_token'];
        }else {
            $accessToken = $headers->get('x-access_token');
        }

        // se obtiene el access token (guion medio) 
        if(empty($accessToken)){

            if(isset($_GET['access-token'])){
                $accessToken=$_GET['access-token'];
            }else {
                $accessToken = $headers->get('x-access-token');
            }
        }

        if(empty($accessToken)){

            if(isset($_POST['access_token'])){
                $accessToken=$_POST['access_token'];
            }
        }

        Yii::$app->api->deleteAccessToken($accessToken);


    }
}
