<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\LoginForm;



class RestController extends Controller
{

    public $request;

    public $enableCsrfValidation = false;

    public $headers;


    public function behaviors()
    {
        $behaviors = parent::behaviors();
        
        // seguridad que indica que metodos o desde que sitios se puede acceder a nuestra api
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                // restrige el acceso a las paginas mencionadas
                // * permite acceso a todo
                'Origin' => ['*'],
                // 'Access-Control-Allow-Origin' => ['*', 'http://haikuwebapp.local.com:81','http://localhost:81'],
                // indica los metodos que son permitidos recibir
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],

                // indica los headers que son permitidos recibir
                // * permite todo
                'Access-Control-Request-Headers' => ['*'],

                //Allow OPTIONS caching
                'Access-Control-Allow-Credentials' => null,

                
                'Access-Control-Max-Age' => 86400,

                //Permitir que el encabezado X-Pagination-Current-Page quede expuesto al navegador
                'Access-Control-Expose-Headers' => []
            ]

        ];
        return $behaviors;
    }

    public function init()
    {
        // lee un documento json 
        //$this->request = json_decode(file_get_contents('php://input'), true);

        // lee un request, obtiene los parametros enviados por la peticion
        $this->request = Yii::$app->request->bodyParams;

        //  verifica que el reques sea un array y sea diferente de null
        if($this->request && !is_array($this->request)){
            Yii::$app->api->sendFailedResponse(['Invalid Json']);

        }

    }

}


