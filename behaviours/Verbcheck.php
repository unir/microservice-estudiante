<?php
namespace app\behaviours;

use Yii;

/**
 * Created by PhpStorm.
 * User: sirink
 * Date: 29/04/15
 * Time: 2:15 AM
 */
class Verbcheck extends \yii\filters\VerbFilter
{


    /**
     * @param ActionEvent $event
     * @return boolean
     * @throws MethodNotAllowedHttpException when the request method is not allowed.
     */
    public function beforeAction($event)
    {
        // se obtiene el nombre de la accion
        $action = $event->action->id;

        // se obtiene los metodos permitidos por la accion que se solicita
        // actionIndex => solo GET o POST
        if (isset($this->actions[$action])) {
            $verbs = $this->actions[$action];
        }elseif (isset($this->actions['*'])) {
            $verbs = $this->actions['*'];
        }else {
            // retorna que el metodo es permitdo
            return $event->isValid;
        }

        // se obtiene el metodo por el cual se esta solitando la accion
        $verb = Yii::$app->getRequest()->getMethod();

        // convierte todos los metodos permitidos a mayusculas para posteriormente hacer la comparacion
        $allowed = array_map('strtoupper', $verbs);
        
        // verifica si el metodo utilizado para solicitar la accion esta entre los metodos permitidos
        if (!in_array($verb, $allowed)) {
            $event->isValid = false;
            // http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.7
            Yii::$app->getResponse()->getHeaders()->set('Allow', implode(', ', $allowed));

            Yii::$app->api->sendFailedResponse('Method Not Allowed. This url can only handle the following request methods: ' . implode(', ', $allowed) . '.');

        }
        // retorna true si el metodo es permitido
        return $event->isValid;
    }

}