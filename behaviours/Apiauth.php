<?php
namespace app\behaviours;
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

//namespace yii\filters\auth;
use Yii;
use yii\filters\auth\AuthMethod;
use app\models\AccessTokens;

/**
 * QueryParamAuth is an action filter that supports the authentication based on the access token passed through a query parameter.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Apiauth extends AuthMethod
{
    /**
     * @var string the parameter name for passing the access token
     */
    public $tokenParam = 'access-token';

    // lista de acciones que no necesitan verificacion
    public $exclude = [];
    public $callback = [];


    /*
        Funcion que permite autenticar la peticion a travez de access token
    */
    public function authenticate($user, $request, $response){
        // obtiene los encabezados de la peticion, en caso de
        // que el access token venga por el encabezado 
        $headers = Yii::$app->getRequest()->getHeaders();

        //se inicializa token en null
        $accessToken=NULL;

        // se obtiene el access_token
        if(isset($_GET['access_token'])){
            $accessToken=$_GET['access_token'];
        }else {
            $accessToken = $headers->get('x-access_token');
        }

        // se obtiene el access token (guion medio) 
        if(empty($accessToken)){

            if(isset($_GET['access-token'])){
                $accessToken=$_GET['access-token'];
            }else {
                $accessToken = $headers->get('x-access-token');
            }
        }

        if(empty($accessToken)){
            if(isset($_POST['access_token'])){
                $accessToken=$_POST['access_token'];
            }
            if(isset($_POST['access-token'])){
                $accessToken=$_POST['access-token'];
            }
        }

        // verifica que el token sea un string
        if (is_string($accessToken)) {
            // se realiza el login
            //$identity = $user->loginByAccessToken($accessToken, get_class($this));
            $modelAT =  AccessTokens::findOne(['token'=>$accessToken]);
            //var_dump($modelAT);
            //exit;
            if (isset($modelAT)) {
                // si existe se devuelve el usuario
                return True;
            }
        }

        // el token es invalido
        if ($accessToken !== null) {
            Yii::$app->api->sendFailedResponse(['Invalid Access token']);
        }

        //token vacio, no se recibio token
        return null;
    }


    /*
        realiza validaciones antes de ejecutar la accion, se ejecuta automaticamente

    */
    public function beforeAction($action){

        // $action->id => indica la accion que se esta solicitando
        // pregunta si la accion solicitada, se encuentra dentro de la lista de excluidos
        if (in_array($action->id, $this->exclude) && !isset($_GET['access-token'])){
            return true;
        }

        // 
        if (in_array($action->id, $this->callback) && !isset($_GET['access-token'])){
            return true;
        }

        //
        $response = $this->response ?: Yii::$app->getResponse();

        // devuelve identity, si el access token correcto
        $identity = $this->authenticate(
            $this->user ?: Yii::$app->getUser(),
            $this->request ?: Yii::$app->getRequest(),
            $response
        );

        // 5!="5"   false
        // 5!=="5"  true
        // Compara si identity es diferente nulll
        if ($identity == True) {
            return true;
        } else {
            $this->challenge($response);
            $this->handleFailure($response);

            Yii::$app->api->sendFailedResponse('Invalid Request');
        }
    }

    /**
     * @inheritdoc
     */
    public function handleFailure($response)
    {
        Yii::$app->api->sendFailedResponse('Invalid Access token');
        //throw new UnauthorizedHttpException('You are requesting with an invalid credential.');
    }

}
