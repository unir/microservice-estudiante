<?php

namespace app\searchs;

use app\models\Escuela;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Estudiante;

/**
 * ProvinciaSearch represents the model behind the search form of `app\models\Provincia`.
 */

class EstudianteSearch extends Estudiante
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['IdProvincia'], 'integer'],
            //[['Provincia', 'codProvincia', 'fechaCreacion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $page = Yii::$app->getRequest()->getQueryParam('page');
        $limit = Yii::$app->getRequest()->getQueryParam('limit');
        $order = Yii::$app->getRequest()->getQueryParam('order');

        $search = Yii::$app->getRequest()->getQueryParam('search');

        if(isset($search)){
            $params=$search;
        }



        $limit = isset($limit) ? $limit : 10;
        $page = isset($page) ? $page : 1;


        $offset = ($page - 1) * $limit;

        $query = Estudiante::find()
            ->asArray(true)
            ->limit($limit)
            ->offset($offset);

        if(isset($params['vc_Cedula'])) {
            $query->andFilterWhere(['like','vc_Cedula', $params['vc_Cedula']]);
        }

        if(isset($params['vc_Matricula'])) {
            $query->andFilterWhere(['vc_Matricula'=> $params['vc_Matricula']]);
        }

        if(isset($params['vc_Nombres'])) {
            $query->andFilterWhere(['like','vc_Nombres', $params['vc_Nombres']]);
        }

        if(isset($params['vc_Apellidos'])) {
            $query->andFilterWhere(['like','vc_Apellidos', $params['vc_Apellidos']]);
        }
        if(isset($params['i_FK_Id_Carrera'])) {
            $query->andFilterWhere(['like','i_FK_Id_Carrera', $params['i_FK_Id_Carrera']]);
        }

        if(isset($order)){
            $query->orderBy($order);
        }

        $additional_info = [
            'page' => $page,
            'size' => $limit,
            'totalCount' => (int)$query->count()
        ];

        $data=[];

        foreach ($query->all() as $modelRequest) {
            $modelRequest['vc_Cedula']= (string) $modelRequest['vc_Cedula'];
            array_push($data, $modelRequest);
        }

        return [
            'data' => $data,
            'info' => $additional_info
        ];
    }
    public function searchHoras($params)
    {

            $escuelas = Escuela::Find();
            $id=[];
            foreach ($escuelas as $key => $value) {
                $id[] = $value['Id_Facultad'];
            }

        $page = Yii::$app->getRequest()->getQueryParam('page');
        $limit = Yii::$app->getRequest()->getQueryParam('limit');
        $order = Yii::$app->getRequest()->getQueryParam('order');

        $search = Yii::$app->getRequest()->getQueryParam('search');

        if(isset($search)){
            $params=$search;
        }



        $limit = isset($limit) ? $limit : 10;
        $page = isset($page) ? $page : 1;


        $offset = ($page - 1) * $limit;

        $query = Estudiante::find()
            ->asArray(true)
        //    ->limit($limit)
            ->offset($offset);

        /*if(isset($params['vc_Cedula'])) {
            $query->andFilterWhere(['like','vc_Cedula', $params['vc_Cedula']]);
        }

        if(isset($params['vc_Matricula'])) {
            $query->andFilterWhere(['vc_Matricula'=> $params['vc_Matricula']]);
        }

        if(isset($params['vc_Nombres'])) {
            $query->andFilterWhere(['like','vc_Nombres', $params['vc_Nombres']]);
        }

        if(isset($params['vc_Apellidos'])) {
            $query->andFilterWhere(['like','vc_Apellidos', $params['vc_Apellidos']]);
        }

        if(isset($params['i_FK_Id_Carrera'])) {
            $query->andFilterWhere(['like','i_FK_Id_Carrera', $params['i_FK_Id_Carrera']]);
        }*/

        $query->andFilterWhere(['like','i_Graduado', '2']);

        $query->andFilterWhere(['and',['=','i_Limite_Hora',120],['<','i_Horas_Vinculacion',120]]);

        $query->orFilterWhere(['and',['=','i_Limite_Hora',160],['<','i_Horas_Vinculacion',160]]);

        $query->andFilterWhere(['in','i_FK_Id_Carrera', $id]);

        if(isset($order)){
            $query->orderBy($order);
        }

        $additional_info = [
            'page' => $page,
            'size' => $limit,
            'totalCount' => (int)$query->count()
        ];

        $data=[];

        foreach ($query->all() as $modelRequest) {
            $modelRequest['vc_Cedula']= (string) $modelRequest['vc_Cedula'];
            array_push($data, $modelRequest);
        }

        return [
            'data' => $data,
            'info' => $additional_info
        ];
    }
}
