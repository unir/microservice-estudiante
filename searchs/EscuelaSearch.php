<?php

namespace app\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Escuela;
use yii\httpclient\Client;
use yii\data\ArrayDataProvider;

/**
 * ClienteSearch represents the model behind the search form about `frontend\models\Cliente`.
 */
class EscuelaSearch extends Escuela
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['NombreCompleto'], 'string'],
            //[['Escuela'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }



    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $client = new Client();
        $param = Yii::$app->params['escuela-search'];
        $token = $param['token'];
        $url = $param['url'];

        $this->load($params);

        if (!$this->validate()) {
            //return $dataProvider;
        }


        $arraySearch["access-token"]=$token;
        $arraySearch["limit"]=25;
        $arraySearch["page"]=isset($params["page"]) ? $params["page"] : 1;
        //$arraySearch["limit"]=isset($params["limit"]) ? $params["limit"] : 10;
        if(isset($params["EscuelaSearch"])) {
            foreach ($params["EscuelaSearch"] as $campo => $value) {
                $arraySearch["search"][$campo] = $value;
            }
        }

        $response=[];
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl($url)
            ->addHeaders(['content-type' => 'application/json'])
            ->setData($arraySearch)
            ->send();

        $dataprovider = new ArrayDataProvider([
            'allModels' => $response->data['data'],
            'sort' => [
                'attributes' => [
                    'Id_Facultad' ,
                    'Especialidad' ,
                    'Especialidad_Duracion' ,
                    'lugar' ,
                    'CarreraGrupo' ,
                    'TipodeCarrera' ,
                    'nivelFormacion' ,
                    'formacion' ,
                    'Duracion' ,
                    'Duracion2' ,
                    'Titulo' ,
                    'Modalidad' ,
                    'codCarrera' ,
                    'codCarrera2' ,
                    'Estado' ,
                ],
            ],
            'pagination' => [
                'page'=>0,
                'pageSize'=>25
            ],
            'totalCount'=>$response->data["totalCount"]
        ]);

        return $dataprovider;
    }
}
