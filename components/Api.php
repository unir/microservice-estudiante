<?php
namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use app\models\User;

use app\models\AuthorizationCodes;
use app\models\AccessTokens;

/**
 * Class for app API functions
 */
class Api extends Component
{

    // Permite enviar una respuesta de error
    public function sendFailedResponse($message)
    {
        // pone en el header el numero del error
        $this->setHeader(400);

        // responde via json el status, error y el mensaje del error
        \Yii::$app->response->data = array('status' => 0, 'error_code' => 400, 'errors' => $message);

        //finaliza la ejecucion de la api
        Yii::$app->end();
    }

    // Permite enviar una respuesta a una peticion
    public function sendSuccessResponse($data = false,$additional_info = false)
    {
        // envia en el header el codigo 200 de exitoso
        $this->setHeader(200);

        // se crea el array de respuesta
        $response = [];
        $response['status'] = 1;

        // verifica si se ha recibido algun dato de respuesta y es asi, lo agrega al response
        if (is_array($data))
            $response['data'] = $data;

        // se añade informacion adicional en caso de existir
        if ($additional_info) {
            $response = array_merge($response, $additional_info);
        }

        // se codifica la respuesta en formato json
        //$response = Json::encode($response, JSON_PRETTY_PRINT);

        // ??
        if (isset($_GET['callback'])) {
            /* this is required for angularjs1.0 client factory API calls to work */
            $response = $_GET['callback'] . "(" . $response . ")";

            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            \Yii::$app->response->data = $response;
            //return $response;
        } else {
            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            \Yii::$app->response->data = $response;
            //return $response;
        }

        Yii::$app->end();

    }

    // Setea el header de la respuesta
    protected function setHeader($status)
    {
        // obtiene el la respuesta en base al numero del status
        //$text = $this->_getStatusCodeMessage($status);

        //Yii::$app->response->setStatusCode($status, $text);
        Yii::$app->response->setStatusCode($status);
        //$headers = Yii::$app->response->headers;

        // add a Pragma header. Existing Pragma headers will NOT be overwritten.
        //$headers->add('Access-Control-Allow-Origin', '*');

        // set a Pragma header. Any existing Pragma headers will be discarded.
        //$headers->set('Pragma', 'no-cache');


        // $status_header = 'HTTP/1.1 ' . $status . ' ' . $text;
        // $content_type = "application/json; charset=utf-8";


        // header($status_header);
        // header('Content-type: ' . $content_type);
        // header('X-Powered-By: ' . "Your Company <www.mywebsite.com>");
        // header('Access-Control-Allow-Origin:*');


    }

    // obtiene el la respuesta en base al numero del status
    protected function _getStatusCodeMessage($status)
    {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    // permite crear el codigo de la autorizacion en base al id usuario
    public function createAuthorizationCode($user_id)
    {
        $model = new AuthorizationCodes;

        // obtiene un id unico de 13 caracteres basado en los microsegundos,
        // adicionalmente codificado en md5
        $model->code = md5(uniqid());

        // genera un codigo de autorizacion que expira en 5 minutos
        $model->expires_at = time() + (60 * 5);

        // se establece el id usuario
        $model->user_id = $user_id;

        //
        // if (isset($_SERVER['HTTP_X_HAIKUJAM_APPLICATION_ID']))
        //     $app_id = $_SERVER['HTTP_X_HAIKUJAM_APPLICATION_ID'];
        // else
        //     $app_id = null;

        //$model->app_id = $app_id;

        $model->app_id = null;

        $model->created_at = time();

        $model->updated_at = time();

        // guarda la autorizacion sin realizar ninguna validacion
        $model->save(false);

        return ($model);

    }

    /*
        Crea un access token
    */
    public function createAccesstoken($authorization_code)
    {

        // se busca el codigo de la autorizacion
        $auth_code = AuthorizationCodes::findOne(['code' => $authorization_code]);

        $model = new AccessTokens();

        // obtiene un id unico de 13 caracteres basado en los microsegundos,
        // adicionalmente codificado en md5
        $model->token = md5(uniqid());

        // se establece el codigo de la autorizacion
        $model->auth_code = $auth_code->code;

        // el token expira en 60 dias
        $model->expires_at = time() + (60 * 60 * 24 * 60); // 60 days

        // se establece el user id de la autorizacin
        $model->user_id = $auth_code->user_id;

        $model->created_at = time();

        $model->updated_at = time();

        // guarda la autorizacion sin realizar ninguna validacion
        $model->save(false);

        return ($model);

    }

    // se refresca el access token
    public function refreshAccesstoken($token)
    {
        $access_token = AccessTokens::findOne(['token' => $token]);
        if ($access_token) {

            $access_token->delete();
            $new_access_token = $this->createAccesstoken($access_token->auth_code);
            return ($new_access_token);
        } else {

            Yii::$app->api->sendFailedResponse("Invalid Access token2");
        }
    }

    // elimina un access token
    public function deleteAccesstoken($token)
    {
        $access_token = AccessTokens::findOne(['token' => $token]);
        if ($access_token) {

            $access_token->delete();
            Yii::$app->api->sendSuccessResponse(["Logged Out Successfully"]);

        } else {

            Yii::$app->api->sendFailedResponse("Invalid Request");
        }
    }


}
