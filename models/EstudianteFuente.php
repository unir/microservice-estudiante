<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vDatosPersonales".
 *
 * @property string $vc_Cedula
 * @property string $dt_Ing
 * @property string $vc_Matricula
 * @property string $vc_Tipo_Identificacion
 * @property string $vc_Apellidos
 * @property string $vc_Nombres
 * @property int $b_Categoria
 * @property string $vc_Sexo
 * @property string $dt_Nacimiento
 * @property string $vc_Lugar_Nacimiento
 * @property string $i_Id_Provincia
 * @property string $vc_Direccion
 * @property string $vc_Ciudad
 * @property string $vc_Telefono
 * @property string $vc_Fax
 * @property string $vc_Nacionalidad
 * @property string $vc_Colegio
 * @property string $vc_Ciudad_Colegio
 * @property string $vc_Provincia_Colegio
 * @property string $vc_Especialidad
 * @property int $i_Calificacion
 * @property string $vc_Otros_Titulos
 * @property int $b_Publicar
 * @property string $vc_Telefono_Familiar
 * @property string $vc_Colegio_Procedencia
 * @property string $vc_Titulo_Egresado
 * @property string $vc_Tel_Familiar
 * @property string $vc_Correo_Institucional
 * @property string $vc_Correo_Personal
 * @property string $vc_Etnia
 * @property string $vc_Celular
 * @property string $vc_Parroquia
 * @property string $dt_Creacion
 * @property string $vc_Canton
 * @property string $vc_Carnet_Conadis
 * @property string $dt_Actualizacion
 * @property string $vc_Cuenta_Facebook
 * @property string $vc_Cuenta_Twitter
 * @property int $i_Id_Estado_Civil
 */
class EstudianteFuente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vDatosPersonales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vc_Cedula'], 'required'],
            [[
                'vc_Cedula', 
                'vc_Matricula', 
                'vc_Tipo_Identificacion', 
                'vc_Apellidos', 
                'vc_Nombres', 
                'vc_Sexo', 
                'vc_Lugar_Nacimiento', 
                'i_Id_Provincia', 
                'vc_Direccion', 
                'vc_Ciudad', 
                'vc_Telefono', 
                'vc_Fax', 
                'vc_Nacionalidad', 
                'vc_Colegio', 
                'vc_Ciudad_Colegio', 
                'vc_Provincia_Colegio', 
                'vc_Especialidad', 
                'vc_Otros_Titulos', 
                'vc_Telefono_Familiar', 
                'vc_Colegio_Procedencia', 
                'vc_Titulo_Egresado', 
                'vc_Tel_Familiar', 
                'vc_Correo_Institucional', 
                'vc_Correo_Personal', 
                'vc_Etnia', 
                'vc_Celular', 
                'vc_Parroquia', 
                'vc_Canton', 
                'vc_Carnet_Conadis', 
                'vc_Cuenta_Facebook', 
                'vc_Cuenta_Twitter'], 'string'],
            [['dt_Ing', 'dt_Nacimiento', 'dt_Creacion', 'dt_Actualizacion'], 'safe'],
            [['b_Categoria', 'i_Calificacion', 'b_Publicar', 'i_Id_Estado_Civil', 'i_Graduado'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vc_Cedula' => 'Vc  Cedula',
            'dt_Ing' => 'Dt  Ing',
            'vc_Matricula' => 'Vc  Matricula',
            'vc_Tipo_Identificacion' => 'Vc  Tipo  Identificacion',
            'vc_Apellidos' => 'Vc  Apellidos',
            'vc_Nombres' => 'Vc  Nombres',
            'b_Categoria' => 'B  Categoria',
            'vc_Sexo' => 'Vc  Sexo',
            'dt_Nacimiento' => 'Dt  Nacimiento',
            'vc_Lugar_Nacimiento' => 'Vc  Lugar  Nacimiento',
            'i_Id_Provincia' => 'I  Id  Provincia',
            'vc_Direccion' => 'Vc  Direccion',
            'vc_Ciudad' => 'Vc  Ciudad',
            'vc_Telefono' => 'Vc  Telefono',
            'vc_Fax' => 'Vc  Fax',
            'vc_Nacionalidad' => 'Vc  Nacionalidad',
            'vc_Colegio' => 'Vc  Colegio',
            'vc_Ciudad_Colegio' => 'Vc  Ciudad  Colegio',
            'vc_Provincia_Colegio' => 'Vc  Provincia  Colegio',
            'vc_Especialidad' => 'Vc  Especialidad',
            'i_Calificacion' => 'I  Calificacion',
            'vc_Otros_Titulos' => 'Vc  Otros  Titulos',
            'b_Publicar' => 'B  Publicar',
            'vc_Telefono_Familiar' => 'Vc  Telefono  Familiar',
            'vc_Colegio_Procedencia' => 'Vc  Colegio  Procedencia',
            'vc_Titulo_Egresado' => 'Vc  Titulo  Egresado',
            'vc_Tel_Familiar' => 'Vc  Tel  Familiar',
            'vc_Correo_Institucional' => 'Vc  Correo  Institucional',
            'vc_Correo_Personal' => 'Vc  Correo  Personal',
            'vc_Etnia' => 'Vc  Etnia',
            'vc_Celular' => 'Vc  Celular',
            'vc_Parroquia' => 'Vc  Parroquia',
            'dt_Creacion' => 'Dt  Creacion',
            'vc_Canton' => 'Vc  Canton',
            'vc_Carnet_Conadis' => 'Vc  Carnet  Conadis',
            'dt_Actualizacion' => 'Dt  Actualizacion',
            'vc_Cuenta_Facebook' => 'Vc  Cuenta  Facebook',
            'vc_Cuenta_Twitter' => 'Vc  Cuenta  Twitter',
            'i_Id_Estado_Civil' => 'I  Id  Estado  Civil',
            'i_Graduado' => 'Graduado'
        ];
    }

    public static function primaryKey()
    {
        return ['vc_Cedula'];
    }
}


