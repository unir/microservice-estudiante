<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vProvincias".
 *
 * @property int $IdProvincia
 * @property string $Provincia
 * @property string $codProvincia
 * @property string $fechaCreacion
 */
class LimiteHora extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function collectionName()
    {
        return ['vinculacion','limite_hora'];
    }


    public function attributes()
    {
        return [
            '_id',
            'dt_Inicio',
            'dt_Fin',
            'limite'
        ];
    }

    public function rules()
    {
        return [
            [['dt_Inicio','dt_Fin','limite'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dt_Inicio' => 'Fecha de Inicio',
            'dt_Fin' => 'Fecha de Finalización',
            'limite' => 'Número de horas'
        ];
    }
}
