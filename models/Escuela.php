<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\httpclient\Client;


/**
 * ContactForm is the model behind the contact form.
 */
class Escuela extends Model
{
    public $Id_Facultad ;
    public $Especialidad ;
    public $Especialidad_Duracion ;
    public $lugar ;
    public $CarreraGrupo ;
    public $TipodeCarrera ;
    public $nivelFormacion ;
    public $formacion ;
    public $Duracion ;
    public $Duracion2 ;
    public $Titulo ;
    public $Modalidad ;
    public $codCarrera ;
    public $codCarrera2 ;
    public $Estado ;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [[
                'Id_Facultad' ,
                'Especialidad' ,
                'Especialidad_Duracion' ,
                'lugar' ,
                'CarreraGrupo' ,
                'TipodeCarrera' ,
                'nivelFormacion' ,
                'formacion' ,
                'Duracion' ,
                'Duracion2' ,
                'Titulo' ,
                'Modalidad' ,
                'codCarrera' ,
                'codCarrera2' ,
                'Estado' ,
            ], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id_Facultad' => 'Id  Facultad',
            'Especialidad' => 'Especialidad',
            'Especialidad_Duracion' => 'Especialidad + Duracion',
            'lugar' => 'Lugar',
            'CarreraGrupo' => 'Carrera Grupo',
            'TipodeCarrera' => 'Tipo de Carrera',
            'nivelFormacion' => 'Nivel de Formación',
            'formacion' => 'Formación',
            'Duracion' => 'Duración',
            'Duracion2' => 'Duración 2',
            'Titulo' => 'Titulo',
            'Modalidad' => 'Modalidad',
            'codCarrera' => 'Cod Carrera',
            'codCarrera2' => 'Cod Carrera 2',
            'Estado' => 'Estado',
        ];
    }

    public function getTotalCount(){
        $client = new Client();
        $param = Yii::$app->params['escuela-search'];
        $token = $param['token'];
        $url = $param['url'];

        $arraySearch["access-token"]=$token;

        $response=[];
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl($url)
            ->addHeaders(['content-type' => 'application/json'])
            ->setData($arraySearch)
            ->send();

        return $response->data["totalCount"];
    }

    public function find($search=[]){
        $client = new Client();
        $param = Yii::$app->params['escuela-search'];
        $token = $param['token'];
        $url = $param['url'];

        $arraySearch["access-token"]=$token;
        $arraySearch["search"] = $search;
        $arraySearch["limit"] = Escuela::getTotalCount();

        $response=[];
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl($url)
            ->addHeaders(['content-type' => 'application/json'])
            ->setData($arraySearch)
            ->send();

        if($response->data["data"] !=null && count($response->data["data"])>0){
            return $response->data["data"];
        }else{
            return null;
        }
    }

    public function findOne($id){
        $response=$this->find(["Id_Facultad"=>$id]);
        //var_dump($response);
        //exit;
        if($response!=null && count($response)==1){
            $this->attributes=$response[0];
            //$this->_id=$response[0]["_id"];
        }else{
            return [];
        }
    }

    public function findOneID($id){
        $response=$this->find(["_id"=>$id]);
        //var_dump($response);
        //exit;
        if($response!=null && count($response)==1){
            $this->attributes=$response[0];
            //$this->_id=$response[0]["_id"];
        }else{
            return [];
        }
    }
}