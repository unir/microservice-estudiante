<?php

namespace app\models;
//use app\models\LimitHora; 

use Yii;
use yii\httpclient\Client;

/**
 * This is the model class for table "vDatosPersonales".
 *
 * @property string $vc_Cedula
 * @property string $dt_Ing
 * @property string $vc_Matricula
 * @property string $vc_Tipo_Identificacion
 * @property string $vc_Apellidos
 * @property string $vc_Nombres
 * @property int $b_Categoria
 * @property string $vc_Sexo
 * @property string $dt_Nacimiento
 * @property string $vc_Lugar_Nacimiento
 * @property string $i_Id_Provincia
 * @property string $vc_Direccion
 * @property string $vc_Ciudad
 * @property string $vc_Telefono
 * @property string $vc_Fax
 * @property string $vc_Nacionalidad
 * @property string $vc_Colegio
 * @property string $vc_Ciudad_Colegio
 * @property string $vc_Provincia_Colegio
 * @property string $vc_Especialidad
 * @property int $i_Calificacion
 * @property string $vc_Otros_Titulos
 * @property int $b_Publicar
 * @property string $vc_Telefono_Familiar
 * @property string $vc_Colegio_Procedencia
 * @property string $vc_Titulo_Egresado
 * @property string $vc_Tel_Familiar
 * @property string $vc_Correo_Institucional
 * @property string $vc_Correo_Personal
 * @property string $vc_Etnia
 * @property string $vc_Celular
 * @property string $vc_Parroquia
 * @property string $dt_Creacion
 * @property string $vc_Canton
 * @property string $vc_Carnet_Conadis
 * @property string $dt_Actualizacion
 * @property string $vc_Cuenta_Facebook
 * @property string $vc_Cuenta_Twitter
 * @property int $i_Id_Estado_Civil
 */
class Estudiante extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function collectionName()
    {
        return ['vinculacion','estudiante'];
    }


    public function attributes()
    {
        return [
            '_id',
            'vc_Cedula',
            'vc_Matricula',
            'vc_Tipo_Identificacion',
            'vc_Apellidos',
            'vc_Nombres',
            'vc_Sexo',
            'vc_Lugar_Nacimiento',
            'i_Id_Provincia',
            'vc_Direccion',
            'vc_Ciudad',
            'vc_Telefono',
            'vc_Fax',
            'vc_Nacionalidad',
            'vc_Colegio',
            'vc_Ciudad_Colegio',
            'vc_Provincia_Colegio',
            'vc_Especialidad',
            'i_Graduado',
            'vc_Otros_Titulos',
            'vc_Telefono_Familiar',
            'vc_Colegio_Procedencia',
            'vc_Titulo_Egresado',
            'vc_Tel_Familiar',
            'vc_Correo_Institucional',
            'vc_Correo_Personal',
            'vc_Etnia',
            'vc_Celular',
            'vc_Parroquia',
            'vc_Canton',
            'vc_Carnet_Conadis',
            'vc_Cuenta_Facebook',
            'vc_Cuenta_Twitter',
            'dt_Ing',
            'dt_Nacimiento',
            'dt_Creacion',
            'dt_Actualizacion',
            'b_Categoria',
            'i_Calificacion',
            'b_Publicar',
            'i_Id_Estado_Civil',
            'i_FK_Id_Carrera',
            'i_Limite_Hora',
            'i_Horas_Vinculacion',
            'vc_Carrera'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[
              'vc_Cedula',
              'vc_Matricula', 
              'vc_Tipo_Identificacion', 
              'vc_Apellidos', 
              'vc_Nombres', 
              'vc_Sexo', 
              'vc_Lugar_Nacimiento', 
              'i_Id_Provincia', 
              'vc_Direccion', 
              'vc_Ciudad', 
              'vc_Telefono', 
              'vc_Fax', 
              'vc_Nacionalidad', 
              'vc_Colegio', 
              'vc_Ciudad_Colegio', 
              'vc_Provincia_Colegio', 
              'vc_Especialidad',
              'i_Graduado',
              'vc_Otros_Titulos', 
              'vc_Telefono_Familiar', 
              'vc_Colegio_Procedencia', 
              'vc_Titulo_Egresado', 
              'vc_Tel_Familiar', 
              'vc_Correo_Institucional', 
              'vc_Correo_Personal', 
              'vc_Etnia', 
              'vc_Celular', 
              'vc_Parroquia', 
              'vc_Canton', 
              'vc_Carnet_Conadis', 
              'vc_Cuenta_Facebook', 
              'vc_Cuenta_Twitter',
              'dt_Ing', 
              'dt_Nacimiento', 
              'dt_Creacion', 
              'dt_Actualizacion',
              'b_Categoria', 
              'i_Calificacion', 
              'b_Publicar', 
              'i_Id_Estado_Civil',
              'i_FK_Id_Carrera',
              'i_Limite_Hora',
            'i_Horas_Vinculacion'], 'safe'],
            [['i_Horas_Vinculacion', 'i_Limite_Hora'], 'integer'],
            [['vc_Carrera'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vc_Cedula' => 'Vc  Cedula',
            'dt_Ing' => 'Dt  Ing',
            'vc_Matricula' => 'Vc  Matricula',
            'vc_Tipo_Identificacion' => 'Vc  Tipo  Identificacion',
            'vc_Apellidos' => 'Vc  Apellidos',
            'vc_Nombres' => 'Vc  Nombres',
            'b_Categoria' => 'B  Categoria',
            'vc_Sexo' => 'Vc  Sexo',
            'dt_Nacimiento' => 'Dt  Nacimiento',
            'vc_Lugar_Nacimiento' => 'Vc  Lugar  Nacimiento',
            'i_Id_Provincia' => 'I  Id  Provincia',
            'vc_Direccion' => 'Vc  Direccion',
            'vc_Ciudad' => 'Vc  Ciudad',
            'vc_Telefono' => 'Vc  Telefono',
            'vc_Fax' => 'Vc  Fax',
            'vc_Nacionalidad' => 'Vc  Nacionalidad',
            'vc_Colegio' => 'Vc  Colegio',
            'vc_Ciudad_Colegio' => 'Vc  Ciudad  Colegio',
            'vc_Provincia_Colegio' => 'Vc  Provincia  Colegio',
            'vc_Especialidad' => 'Vc  Especialidad',
            'i_Calificacion' => 'I  Calificacion',
            'vc_Otros_Titulos' => 'Vc  Otros  Titulos',
            'b_Publicar' => 'B  Publicar',
            'vc_Telefono_Familiar' => 'Vc  Telefono  Familiar',
            'vc_Colegio_Procedencia' => 'Vc  Colegio  Procedencia',
            'vc_Titulo_Egresado' => 'Vc  Titulo  Egresado',
            'vc_Tel_Familiar' => 'Vc  Tel  Familiar',
            'vc_Correo_Institucional' => 'Vc  Correo  Institucional',
            'vc_Correo_Personal' => 'Vc  Correo  Personal',
            'vc_Etnia' => 'Vc  Etnia',
            'vc_Celular' => 'Vc  Celular',
            'vc_Parroquia' => 'Vc  Parroquia',
            'dt_Creacion' => 'Dt  Creacion',
            'vc_Canton' => 'Vc  Canton',
            'vc_Carnet_Conadis' => 'Vc  Carnet  Conadis',
            'dt_Actualizacion' => 'Dt  Actualizacion',
            'vc_Cuenta_Facebook' => 'Vc  Cuenta  Facebook',
            'vc_Cuenta_Twitter' => 'Vc  Cuenta  Twitter',
            'i_Id_Estado_Civil' => 'I  Id  Estado  Civil',
            'i_Graduado'=>'I Graduado',
            'i_Horas_Vinculacion' => 'I Horas Vinculacion'
        ];
    }

    public function setLimiteHora(){

        $dt_Ing = date_create($this->dt_Ing);
        $listLimitHora= LimiteHora::find()->orderby('dt_Inicio asc')->asArray(true)->all();
        //agregando nuevos atributos
        $tv=Estudiante::getTotalHoras($this->vc_Matricula);
        $c=Escuela::find(["Id_Facultad"=>$this->i_FK_Id_Carrera]);
        $this->i_Horas_Vinculacion=(int)$tv;
        $this->vc_Carrera=$c[0]['Especialidad'];
        //---------------
        foreach ($listLimitHora as $limit) {
          if($limit['dt_Fin']!=''){
            $dt_Inicio = date_create($limit['dt_Inicio']);
            $dt_Fin = date_create($limit['dt_Fin']);
            
            if($dt_Ing>=$dt_Inicio && $dt_Ing<$dt_Fin){
               $this->i_Limite_Hora=(int)$limit['limite'];
               return true;
            }
          }elseif($dt_Ing>=$dt_Inicio && $limit['dt_Fin']==''){
            $this->i_Limite_Hora=(int)$limit['limite'];
            return true;
          }else{
            $this->i_Limite_Hora='';
            return true;
          }
        }

        return false;
    }

    public function getLastMatricula($correo_institucional){
      if($correo_institucional!=null){
          $estudiante = Estudiante::find()->where(['vc_Correo_Institucional'=>$correo_institucional])->orderby((int)'vc_Matricula desc')->asArray()->one();

          if($estudiante!=null)
              return $estudiante;
      }
      
      return [];
          
    }

    public function getTotalHoras($listIdentificadores){
        $client = new Client();
        $param = Yii::$app->params['estudiante-search-horas'];
        $token = $param['token'];
        $url = $param['url'];
        $arraySearch["access-token"]=$token;
        $arraySearch["listIdentificadores[0]"] = $listIdentificadores;
        $response=[];
        $response = $client->createRequest()
            ->setMethod('GET')
            ->setUrl($url)
            ->addHeaders(['content-type' => 'application/json'])
            ->setData($arraySearch)
            ->send();
        //print_r($response);
        return $response->data['data']['total'];
    }

    public function getEstudiantesNuevosPeriodo($param){
        $search=[];

        /*
            Busquedas y filtros
        */
        if(isset($param["vc_Anio"]) && !empty($param["vc_Anio"])){
            $search['vc_Anio']=$param["vc_Anio"];
        }

        if(isset($param["vc_Periodo"]) && !empty($param["vc_Periodo"])){
            if($param["vc_Periodo"]=="I"){
                $search['vc_Mes_1']="03";
                $search['vc_Mes_2']="07";
            }elseif($param["vc_Periodo"]=="II"){
                $search['vc_Mes_1']="08";
                $search['vc_Mes_2']="12";
            }
        }



        /*********************/
        if(count($search)>0){
            $collection = Yii::$app->mongodb->getCollection('estudiante');
            $lista = $collection->aggregate([
                [
                    '$project' =>[
                        'date' => [
                            '$split'=> ['$dt_Ing','-']
                        ],
                        'i_FK_Id_Carrera'=>1
                    ]
                ],[
                    '$project' => [
                        'vc_Anio' => [
                            '$arrayElemAt'=> [ '$date', 0 ]
                        ],
                        'vc_Mes' => [
                            '$arrayElemAt'=> [ '$date', 1 ]
                        ],
                        'vc_Dia' => [
                            '$arrayElemAt'=> ['$date', 2]
                        ],
                        'i_FK_Id_Carrera'=> 1
                    ]
                ],[
                    '$match' => [
                        'vc_Anio'=> [ '$eq'=>$search['vc_Anio'] ],
                        'vc_Mes'=> [ '$gte'=> $search['vc_Mes_1'], '$lte'=> $search['vc_Mes_2']]
                    ]
                ],[
                    '$group'=>[
                        '_id' => '$i_FK_Id_Carrera',
                        'num_estudiante'=> [
                                '$sum' => 1
                        ]
                    ]
                ],[
                     '$lookup' => [
                        'from'=> 'escuela',
                        'localField'=> '_id',
                        'foreignField'=> 'Id_Facultad',
                        'as'=> 'escuela'
                     ]
                ],[
                    '$unwind' => '$escuela'
                ], [
                     '$project' => [
                         'vc_Nombre_Escuela' => '$escuela.Especialidad',
                         'num_estudiante'=> [ '$toInt' => '$num_estudiante' ]
                     ]
                ],
                [
                    '$sort' => [
                        'num_estudiante' => -1,
                    ]
                ]
            ]);
        }else{
            $lista=[];
        }


        return $lista;
    }

}


